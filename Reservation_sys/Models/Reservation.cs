﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation_sys.Models
{
    public class Reservation
    {
        [Key]
        public int reservation_id { get; set; }
        public DateTime reserve_date { get; set; }
        public string title { get; set; }
        public string required_files { get; set; }
        public string description { get; set; }
        [ForeignKey("user")]
        public string Id;
        public ApplicationUser user { get; set; }


    }
}
