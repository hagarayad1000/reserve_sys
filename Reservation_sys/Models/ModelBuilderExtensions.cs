﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation_sys.Models
{
    public static class ModelBuilderExtensions
    {
        
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole
                    {
                        Name="patient",
                        NormalizedName="PATIENT"
                    }
                    ,new IdentityRole
                    {
                         Name="doctor",
                         NormalizedName="DOCTOR"
                    }
                );
        }
    }
}
