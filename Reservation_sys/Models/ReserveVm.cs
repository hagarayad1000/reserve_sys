﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation_sys.Models
{
    public class ReserveVm
    {
        [Required(ErrorMessage ="Date is required")]
        [DataType(DataType.DateTime)]
        public DateTime reserve_date { get; set; }
        public string title { get; set; }
       
        public IFormFile required_files { get; set; }
        [Required(ErrorMessage ="description is required")]
        public string description { get; set; }
    }
}
