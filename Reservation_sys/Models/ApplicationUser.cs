﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation_sys.Models
{
    public class ApplicationUser:IdentityUser
    {
        public virtual List<Reservation> reservations { get; set; }

    }
}
