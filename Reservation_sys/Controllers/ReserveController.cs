﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Reservation_sys.Models;

namespace Reservation_sys.Controllers
{
    [Authorize]
    public class ReserveController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AppDbContext _db;
        public ReserveController(IHostingEnvironment hostingEnvironment,UserManager<ApplicationUser> userManager, AppDbContext db)
        {
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
            _db = db;
        }

        [HttpGet]
        public async Task<IActionResult> index()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            string userId = user.Id;
            List<Reservation> reserve_list = new List<Reservation>();
            reserve_list = _db.reservations.Where(x => x.Id == userId).ToList();
            return View(reserve_list);
        }

        [Authorize(Roles = "patient")]
        [HttpGet]
        public IActionResult reserve()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> reserve(ReserveVm model)
        {
            if (ModelState.IsValid)
            {           
                    var user = await _userManager.GetUserAsync(HttpContext.User);
                    string userId = user.Id;
                    string[] splited_txt = model.required_files.FileName.Split('\\');
                    string modified = splited_txt[splited_txt.Length - 1];
                    string file_name = null;
                    if (model.required_files != null)
                    {
                        string uploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, "userUploads");
                        file_name = Guid.NewGuid().ToString() + "_" + modified;
                        string file_path = Path.Combine(uploadFolder, file_name);
                        model.required_files.CopyTo(new FileStream(file_path, FileMode.Create));
                    }
                    Reservation r = new Reservation
                    {
                        description = model.description,
                        title = model.title,
                        reserve_date = model.reserve_date,
                        required_files = file_name,
                        Id = userId
                    };
                    _db.reservations.Add(r);
                    _db.SaveChanges();
                     return RedirectToAction("index");


            }
            return View();
        }
    }
}