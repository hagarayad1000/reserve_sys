﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reservation_sys.Migrations
{
    public partial class update_roles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "4531883c-bb54-44f3-93d3-88ae6508cc21", "78b305c4-7b9d-4217-98c1-42f438a4cce2", "patient", "PATIENT" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "e98ac8b5-971a-4dfb-9444-9cebc2c8857d", "7121f083-36ae-46ea-a71f-6e9cdf3d9fc4", "doctor", "DOCTOR" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4531883c-bb54-44f3-93d3-88ae6508cc21");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "e98ac8b5-971a-4dfb-9444-9cebc2c8857d");
        }
    }
}
