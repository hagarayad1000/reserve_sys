﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reservation_sys.Migrations
{
    public partial class x : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_reservations_AspNetUsers_Id",
                table: "reservations");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "reservations",
                newName: "userId");

            migrationBuilder.RenameIndex(
                name: "IX_reservations_Id",
                table: "reservations",
                newName: "IX_reservations_userId");

            migrationBuilder.AddForeignKey(
                name: "FK_reservations_AspNetUsers_userId",
                table: "reservations",
                column: "userId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_reservations_AspNetUsers_userId",
                table: "reservations");

            migrationBuilder.RenameColumn(
                name: "userId",
                table: "reservations",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_reservations_userId",
                table: "reservations",
                newName: "IX_reservations_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_reservations_AspNetUsers_Id",
                table: "reservations",
                column: "Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
